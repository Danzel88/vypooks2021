import aiofiles
import re


async def censorship_in_messages(message_text: str):
    async with aiofiles.open('bad_words.txt', 'r') as f:
        bad_words = [w.replace('\n', '') for w in await f.readlines()]
        word = re.findall(r'\w+', message_text)
        censor = set(word).intersection(bad_words)
        return censor

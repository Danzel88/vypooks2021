from aiogram.contrib.fsm_storage.memory import MemoryStorage


storage = MemoryStorage()


async def anti_flood(*args, **kwargs):
    message = args[0]
    await message.answer('Слишком часто пишешь, '
                         'отдохни 5 секунд')

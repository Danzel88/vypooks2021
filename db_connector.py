from redis import Redis

redis_client = Redis(host='127.0.0.1', port=6379, db=0)


async def clean_db():
    keys = redis_client.keys()
    redis_client.delete(*keys)


async def write_to_db(messages: dict):
    redis_client.mset(messages)
    print(redis_client.keys())

from aiogram import Bot, Dispatcher, executor, types
from censorship import censorship_in_messages
from setting import API_TOKEN
import random

from answers_list import answers
from db_connector import write_to_db, clean_db
from antiflood import storage, anti_flood
from google_io import sheet, sheet2, sheet3, sheet4, bonus, force_upload
from file_handler import create_message_list, handle_force_upload, get_key_value_pair


bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot, storage=storage)


@dp.message_handler(commands=['clean_db'])
async def handle_clean_db(message: types.Message):
    await clean_db()
    await message.answer(f'База очищена')


@dp.message_handler(commands=['load1', 'load2', 'load3', 'load4'])
async def handle_upload_in_db(message: types.Message):
    if message.text == '/load1':
        data = await get_key_value_pair(sheet)
        await write_to_db(data)
        await message.answer(f'В БД выгружена 1 страница\n{len(data)} сообщений')
    elif message.text == '/load2':
        data = await get_key_value_pair(sheet2)
        await write_to_db(data)
        await message.answer(f'В БД выгружена 2 страница\n{len(data)} сообщений')
    elif message.text == '/load3':
        data = await get_key_value_pair(sheet3)
        await write_to_db(data)
        await message.answer(f'В БД выгружена 3 страница\n{len(data)} сообщений')
    elif message.text == '/load4':
        data = await get_key_value_pair(sheet4)
        await write_to_db(data)
        await message.answer(f'В БД выгружена 4 страница\n{len(data)} сообщений')


@dp.message_handler(commands=['upload'])
async def handle_upload_in_google_sheet(message: types.Message):
    if await handle_force_upload():
        await message.answer(f'Сообщения выгружены в таблицу')
    else:
        await message.answer(f'Нет сообщений для выгрузки')


@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    user_name = message['from']['username']
    await message.reply(f"Привет {user_name}")


@dp.message_handler()
@dp.throttled(anti_flood, rate=5)
async def check_message(message: types.Message):
    # await message.answer(random.choice(answers))   #атоответ
    words_for_check = message.text.lower()
    if len(words_for_check) < 200:
        result_of_checking = await censorship_in_messages(words_for_check)
        if not result_of_checking:
            if await create_message_list(message):
                await message.answer(f'Жди свое сообщение на общем экране')
            else:
                await message.answer(f'Твое собщение попало в дикуо чередь...'
                                     f'только через час попадет на экран')
        else:
            await message.answer(f'НЕ МАТЕРИСЬ!')
    else:
        await message.answer(f'Слишком длинное сообщение. Уложись в 200 символов')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)

import gspread
from google.oauth2.service_account import Credentials


scopes = [
    'https://www.googleapis.com/auth/spreadsheets',
    'https://www.googleapis.com/auth/drive'
]
credentials = Credentials.from_service_account_file(
    '/home/den/.config/gspread/service_account.json',
    scopes=scopes)
gc = gspread.service_account()
sheet = gc.open('bot_message').sheet1

sheet2 = gc.open('bot_message').worksheet("Лист2")
sheet3 = gc.open('bot_message').worksheet("Лист3")
sheet4 = gc.open('bot_message').worksheet("Лист4")
bonus = gc.open('bot_message').worksheet("бонус")
force_upload = gc.open('bot_message').worksheet("Ручная выгрузка")
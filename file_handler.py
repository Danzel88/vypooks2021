import os
import csv
import shutil
from datetime import datetime
from setting import OUTPUT_FILES
from google_io import sheet, sheet2, sheet3, sheet4, bonus, force_upload
from aiogram import types


messages = []


async def copy_file(src, dst):
    shutil.copyfile(src, dst)


async def write_data_in_google_sheets(page, data: list) -> bool:
    """
    We write data (messages from the "messages" list).
    Limitations on the length of the list of messages - 180 pieces.
    :param data: list
    :param page: gspread.models.Worksheet
    """
    page.append_rows(data)
    return True


async def create_message_list(message: types.Message) -> bool:
    """
    Get messages_id, username and message text from messages.
    If the username is not specified, the name is assigned: "Graduate + user_id".
    Then we form a list of the required length (180 messages) from the received data,
    save it in .csv. Then check the first page of the sheet in the google sheets,
    if it is empty, write the resulting list.
    :param message:
    """
    global messages
    message_id = message['message_id']
    if not message['from']['username']:
        user_name = f'Graduate_{message["from"]["id"]}'
    else:
        user_name = message['from']['username']
    message_text = message.text
    if len(messages) <= 3:
        messages.append([message_id, user_name, message_text])
        return True
    else:
        messages.append([message_id, user_name, message_text])
        file_name = f'messages'
        with open(f"{file_name}.csv", 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(messages)
            if not sheet.get_all_values():
                await write_data_in_google_sheets(sheet, messages)
                messages = []
            elif not sheet2.get_all_values():
                await write_data_in_google_sheets(sheet2, messages)
                messages = []
            elif not sheet3.get_all_values():
                await write_data_in_google_sheets(sheet3, messages)
                messages = []
            elif not sheet4.get_all_values():
                await write_data_in_google_sheets(sheet4, messages)
                messages = []
            else:
                await write_data_in_google_sheets(bonus, messages)
        await copy_file(f"{file_name}.csv", f"{OUTPUT_FILES}{file_name}_sheet.csv")
        messages = []


async def handle_force_upload():
    global messages
    if len(messages) > 0:
        await write_data_in_google_sheets(force_upload, messages)
        messages = []
        return True
    else:
        return False


async def get_key_value_pair(page) -> dict:
    all_messages = {}
    row_all_messages = page.get_all_values()
    for message in row_all_messages:
        all_messages[f'{message[0]}:{message[1]}'] = message[2]
    return all_messages
